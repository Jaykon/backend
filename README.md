# Health24

## Routes

### Doctors

```api
 - /d4721ee0acec3a213fdeb021231b/doctors
 POST /register
 POST /authenticate
 POST /verify
 POST /resend
 POST /
 POST /online
 POST /clinic
 POST /update

```

### Patients

```api
 - /d4721ee0acec3a213fdeb021231b/patients
 POST /register
 POST /authenticate
 POST /verify
 POST /resend
 POST /
 PUT /card
 DELETE /card
 POST /pay
 PATCH /pay

```

### Consuming the API

- A user details are sent to the register route, then a token is sent to the user's phone, the token and other details are sent to the verify route, once verified, a key is sent to the user's device which will be required to access the remaining routes.

### Fields required by every route

#### - Doctors

```
   "Content-Type" : "application/json"
```

##### .post /register

```
   {
       "firstname": required,
       "lastname": required,
       "email": required,
       "phonenumber": format(0XXXXXXXXXX) required,
       "password": required
   }

   // return value
   {
       success: true,
       message: "Token sent successfully!"
   }
```

##### .post /authenticate

```
   {
       "phonenumber": format(0XXXXXXXXXX) required,
   }

   // return value
   {
       success: true,
       message: "Token sent successfully!"
   }
```

##### .post /verify

```
   {
       "phonenumber": format(0XXXXXXXXXX) required,
       "token": required,
       "register": 'true' if user is registering 'false' if user is logging-in
   }

   // return value
   {
       success: true,
       key: key,
       message: 'Profile created successfully' if user is registering 'Login successful' if user is logging-in
   }
```

##### .post /resend

```
   {
       "phonenumber": format(0XXXXXXXXXX) required,
   }

   // return value
   {
       success: true,
       message: "Token sent successfully!"
   }

```

##### .post /

```
   // Setting the header
   "Authentication" : "KEY key-sent-to-device",
```

```
   {
       "coordinate": {
           "latitude": format(decimal degrees with minus(-) for south and east),
           "longitude": format(decimal degrees with minus(-) for south and east)
       },
       "notification_token": Firebase Cloud Messaging registration token
       "key": key-sent-to-device,
   }

   // return value
   {
       success: true,
       doctor: object containing the doctor details
   }

```

##### .post /online

```
   // Setting the header
   "Authentication" : "KEY key-sent-to-device",
```

```
    {
       "coordinate": {
           "latitude": format(decimal degrees with minus(-) for south and east),
           "longitude": format(decimal degrees with minus(-) for south and east)
       },
       "key": key-sent-to-device,
   }

   // return value
   {
       success: true,
       message: "You're presently online"
   }

```

##### .post /clinic

```
    // Setting the header
   "Authentication" : "KEY key-sent-to-device",

```

```
    {
        "key": key-sent-to-device,
        "patient_id": required,
        "notes": required,
        "diagnosis": required
    }

    // return value
   {
       success: true,
       message: "Patient clinic notes updated"
   }
```

#### - Patients

```
   "Content-Type" : "application/json"
```

##### .post /register

```
   {
       "firstname": required,
       "lastname": required,
       "email": required,
       "phonenumber": format(0XXXXXXXXXX) required,
       "password": required
   }

   // return value
   {
       success: true,
       message: "Token sent successfully!"
   }
```

##### .post /authenticate

```
   {
       "phonenumber": format(0XXXXXXXXXX) required,
   }

   // return value
   {
       success: true,
       message: "Token sent successfully!"
   }
```

##### .post /verify

```
   {
       "phonenumber": format(0XXXXXXXXXX) required,
       "token": required,
       "register": 'true' if user is registering 'false' if user is logging-in
   }

   // return value
   {
       success: true,
       key: key,
       message: 'Profile created successfully' if user is registering 'Login successful' if user is logging-in
   }
```

##### .post /resend

```
   {
       "phonenumber": format(0XXXXXXXXXX) required,
   }

   // return value
   {
       success: true,
       message: "Token sent successfully!"
   }

```

##### .post /

```
   // Setting the header
   "Authentication" : "KEY key-sent-to-device",
```

```
   {
       "coordinate": {
           "latitude": format(decimal degrees with minus(-) for south and east),
           "longitude": format(decimal degrees with minus(-) for south and east)
       },
       "notification_token": Firebase Cloud Messaging registration token
       "key": key-sent-to-device,
   }

   // return value
   {
       success: true,
       patient: object containing the patient details including nearby doctors
   }

```

##### .put /card

```
    // Setting the header
   "Authentication" : "KEY key-sent-to-device",
```

```
    {
        "cardnumber": required,
        "expiryyear": required,
        "expirymonth": required,
        "customername": required,
        "key": key-sent-to-device,
    }

    // return value
    {
        success: true,
        message: "Card added successfully"
    }
```

##### .delete /card

```
    // Setting the header
   "Authentication" : "KEY key-sent-to-device",
```

```
    {
        "cardnumber": required,
        "expiryyear": required,
        "expirymonth": required,
        "customername": required,
        "key": key-sent-to-device,
    }

    // return value
    {
        success: true,
        message: "Card deleted successfully"
    }
```

##### .post /pay

```
    // Setting the header
   "Authentication" : "KEY key-sent-to-device",
```

```
    {
        "cardnumber": required,
        "expiryyear": required,
        "expirymonth": required,
        "customername": required,
        "cvv": required,
        "amount": required,
        "pin": required,
        "key": key-sent-to-device,
        "embed_token": not required, only sent if present in the user's card details sent by the server 
    }

    // return value if "embed_token" is not present
    {
        success: true,
        reference: payment-ref-sent-to-device,
        message: "message"
    }

    //return value if "embed_token" is present
    {
        success: true, 
        message: 'Consultation completed successfully'
    }
```

##### .patch /pay

```
    // Setting the header
   "Authentication" : "KEY key-sent-to-device",
```

```
    {
        "reference": payment-ref-sent-to-device,
        "otp": otp-sent-to-phonenumber,
        "key": key-sent-to-device,
    }

    // return value
    {
        success: true, 
        message: 'Consultation completed successfully'
    }
```

#### Sockets
- Utilizing the sockets, the header ```x-auth-header``` should be added to the socket whilst trying to establishing a connection

##### Events emitted
- request -> the request event is emitted when a patient makes a request for a doctor, the server processes the event emitted and sends a request to the nearest doctor

Payload sent to doctor
```
    distance: distance-between-doctor-and-patient,
    location: patient-location,
    driving: time-to-arrive-while-driving,
    cycling: time-to-arrive-while-cycling,
    walking: time-to-arrive-while-walking,
    patient_id: patient-id

```

- reject -> the reject event is to be emitted when a doctor who recieves a request rejects the request, when a request is sent to a nearby doctor, the doctor has 30 seconds to decline or accept the request, after 30 seconds the request is automatically rejected

Field required 
```
    patient_id: patient-id-sent-to-the-doctor-in-the-payload
```

- accept -> the accept event is to be emitted when a doctor who recieves a request accepts the request, a payload is then sent to the patient informing that his/her request has been accepted

Field required
```
    patient_id: patient-id-sent-to-the-doctor-in-the-payload
```

Payload sent to patient
```
    
    doctor: {
        coordinate: doctor-location,
        name: doctor-name,
        specialty: doctor-specialty,
        email: doctor-email,
        phonenumber: doctor-phonenumber,
        notification_token: doctor-notification_token
    },
    distance: distance-between-doctor-and-patient,
    timeToArrive: time-to-arrive

```

- move -> the move event is emitted when a doctor moves, this is only to be emitted if the doctor has a patients request, a payload is sent to the patients app informing them of the doctors location

Field required
```
    coordinate: {
        "latitude": required,
        "longitude": required
    },
    patient_id: patient_id sent to the doctors app once he/she accepted the request 
```

Payload sent to patient
```
    coordinate: coordinate of doctor, 
    distance: distance-between-doctor-and-patient,
    timeToArrive: time-to-arrive

```

- cancel -> the cancel event is emitted when a patient cancels a request, this is only to be emitted when a patient clicks on a button displaying a 'CANCEL REQUEST'.

Field required
```
  patient_id: patient_id sent to the patients app once a doctor accepts his/her request
```

Payload sent to doctor
```
  body: a-message-informing-the-doctor-the-request-has-been-cancelled,
  timestamp: time-it-was-cancelled
```

- arrived -> the arrived event is emitted when a doctor clicks on a button 'ARRIVED' when he/she has arrived at the patient's destination.

Field required
```
  patient_id: patient_id: patient_id sent to the doctors app once he/she accepted the request
```

Payload sent to patient
```
  body: a-message-informing-the-patient-the-doctor-has-arrived,
  timestamp: time-he/she-arrived
```

- message -> the message event is emitted when a doctor sends a message to a patient, this only happens when a doctor has accepted a request and is en-route to the patients destination.


Field required
```
  {
    message: message-sent-by-doctor,
    patient_id: patient_id: patient_id sent to the doctors app once he/she accepted the request
  }
```

Payload sent to patient
```
  message: message-sent-by-doctor,
  timestamp: time-message-was-sent
```


- reply -> the reply event is emitted when a patient replies a doctor's initial message sent, this can only happen when a doctor sends a message.

Field required
```
  {
    message: message-sent-by-patient,
    patient_id: patient_id: patient_id sent to the doctors app once he/she accepted the request
  }
```

Payload sent to doctor
```
  message: message-sent-by-patient,
  timestamp: time-message-was-sent
```



